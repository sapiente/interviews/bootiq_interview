<?php


namespace Controllers;


use \Statistic\Reporters\ProductReporter;
use Services\ProductService;
use Utils\ResponseFactory;

class ProductController
{
    /** @var ProductService */
    private $service;
    /** @var ProductReporter */
    private $reporter;

    public function __construct(ProductService $service, ProductReporter $reporter)
    {
        $this->service = $service;
        $this->reporter = $reporter;
    }

    /**
     * @param string $id
     *
     * @return string
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function detail(string $id) : string
    {
        $product = $this->service->findProduct($id);
        $this->reporter->reportProductDetailRequest($id);

        return ResponseFactory::success($product);
    }
}