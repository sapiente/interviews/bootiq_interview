<?php


namespace Statistic\Reporters;


use Statistic\Storage\Storage;

class ProductReporter
{
    CONST PRODUCT_DETAILS_KEY = 'product_details';

    /** @var Storage */
    private $storage;

    public function __construct(Storage $cache)
    {
        $this->storage = $cache;
    }

    public function reportProductDetailRequest(string $id)
    {
        $data = $this->storage->load(self::PRODUCT_DETAILS_KEY);
        $data[$id] = ($data[$id] ?? 0) + 1;
        $this->storage->store(self::PRODUCT_DETAILS_KEY, $data);
    }
    
    public function getProductDetailRequestStat(string $id) : int
    {
        return $this->storage->load(self::PRODUCT_DETAILS_KEY)[$id] ?? 0;
    }
}