<?php


namespace Statistic\Storage;


class ArrayStorage implements Storage
{
    /** @var array */
    private $data = [];

    /**
     * @inheritDoc
     */
    public function load(string $key): array
    {
        return $this->data[$key] ?? [];
    }

    /**
     * @inheritDoc
     */
    public function store(string $key, array $content)
    {
        $this->data[$key] = $content;
    }

}