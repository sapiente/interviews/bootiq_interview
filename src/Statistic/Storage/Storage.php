<?php


namespace Statistic\Storage;


interface Storage
{
    /**
     * Load statistic data under given key
     *
     * @param string $key
     *
     * @return array
     */
    public function load(string $key) : array;

    /**
     * Store statistic data under given key
     *
     * @param string $key
     * @param array $content
     */
    public function store(string $key, array $content);
}