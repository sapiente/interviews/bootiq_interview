<?php


namespace Statistic\Storage;


class FileStorage implements Storage
{
    /** @var string */
    private $file;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    /**
     * @inheritDoc
     */
    public function load(string $key): array
    {
        $data = json_decode(file_get_contents($this->file), true);
        return $data[$key] ?? [];
    }

    /**
     * @inheritDoc
     */
    public function store(string $key, array $data)
    {
        $content = json_decode(file_get_contents($this->file), true) ?: [];
        $content[$key] = $data;
        file_put_contents($this->file, json_encode($content), LOCK_EX);
    }

}