<?php


namespace Utils;


/**
 * Response factory using JSend (@see https://labs.omniti.com/labs/jsend) standard.
 */
class ResponseFactory
{
    private const
        SUCCESS = 'success',
        FAIL = 'fail',
        ERROR = 'error';


    public static function success($data = null) : string
    {
        return self::envelope(self::SUCCESS, null, $data);
    }

    public static function fail($data) : string
    {
        return self::envelope(self::FAIL, null, $data);
    }

    public static function error(string $message) : string
    {
        return self::envelope(self::ERROR, $message, null);
    }

    private static function envelope(string $status, ?string $message, $data) : string
    {
        return json_encode(array_filter(compact('status', 'message', 'data')));
    }
}