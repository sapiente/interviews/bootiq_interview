<?php


namespace Services;


use Psr\SimpleCache\CacheInterface;
use Repositories\Product\ProductRepository;

class ProductService
{
    const PRODUCT_CACHE_KEY = 'product.%s';

    /** @var ProductRepository */
    private $repository;
    /** @var CacheInterface */
    private $cache;

    public function __construct(ProductRepository $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    /**
     * @param string $id
     *
     * @return array
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function findProduct(string $id) : array
    {
        $cacheKey = sprintf(self::PRODUCT_CACHE_KEY, $id);
        $product = $this->cache->get($cacheKey, null);

        if(! $product) {
            $product = $this->repository->find($id);
            $this->cache->set($cacheKey, $product);
        }

        return $product;
    }
}