<?php


namespace Repositories\Product;


use Drivers\MySQL\IMySQLDriver;

class ProductMySQLRepository implements ProductRepository
{
    /**
     * @var IMySQLDriver
     */
    private $driver;

    public function __construct(IMySQLDriver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @inheritdoc
     */
    public function find(string $id) : array
    {
        return $this->driver->findProduct($id);
    }
}