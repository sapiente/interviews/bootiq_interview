<?php


namespace Repositories\Product;


use Drivers\ElasticSearch\IElasticSearchDriver;

class ProductElasticSearchRepository implements ProductRepository
{
    /**
     * @var IElasticSearchDriver
     */
    private $driver;

    public function __construct(IElasticSearchDriver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @inheritdoc
     */
    public function find(string $id) : array
    {
        return $this->driver->findById($id);
    }
}