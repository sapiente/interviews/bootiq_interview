<?php


namespace Repositories\Product;


interface ProductRepository
{
    /**
     * @param string $id
     *
     * @return array
     */
    public function find(string $id) : array;
}