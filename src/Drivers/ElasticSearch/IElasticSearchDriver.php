<?php


namespace Drivers\ElasticSearch;


interface IElasticSearchDriver
{
    /**
     * @param string $id
     *
     * @return array
     */
    public function findById(string $id);
}