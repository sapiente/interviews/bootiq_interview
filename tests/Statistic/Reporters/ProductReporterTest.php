<?php

namespace Tests\Statistic\Reporters;


use PHPUnit\Framework\TestCase;
use Statistic\Reporters\ProductReporter;
use Statistic\Storage\FileStorage;

class ProductReporterTest extends TestCase
{
    /** @var ProductReporter */
    private $reporter;
    /** @var string */
    private $file;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        parent::setUp();
        $this->file = tempnam(__DIR__, 'STAT');
        $this->reporter = new ProductReporter(new FileStorage($this->file));
    }

    /**
     * @inheritDoc
     */
    protected function tearDown()
    {
        parent::tearDown();
        unlink($this->file);
    }


    public function testReportProductDetailRequest()
    {

        $this->reporter->reportProductDetailRequest("1");
        $this->reporter->reportProductDetailRequest("1");
        $this->reporter->reportProductDetailRequest("2");

        $this->assertSame(2, $this->reporter->getProductDetailRequestStat("1"));
        $this->assertSame(1, $this->reporter->getProductDetailRequestStat("2"));
        $this->assertSame(0, $this->reporter->getProductDetailRequestStat("3"));
    }
}
